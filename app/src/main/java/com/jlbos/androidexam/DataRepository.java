package com.jlbos.androidexam;

import com.jlbos.androidexam.api.response.AttractionTaskReponse;
import com.jlbos.androidexam.api.response.GetAttractionTaskResponse;
import com.jlbos.androidexam.api.task.AttractionAsyncTask;
import com.jlbos.androidexam.api.task.GetAttractionAsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class DataRepository implements AttractionTaskReponse, GetAttractionTaskResponse {

    private static DataRepository sInstance;
    private List<String> attractionList;
    private List<String> singleAttraction;
    private AppExecutor appExecutor;

    private MediatorLiveData<List<String>> mObservableAttractions;

    private DataRepository() {
        new AttractionAsyncTask();
    }

    public static DataRepository getInstance(){
        if (sInstance ==  null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository();
                }
            }
        }
        return sInstance;
    }

    public LiveData<List<String>> getAttractions() {
        return mObservableAttractions;
    }

    public List<String> fetchAttraction(final String name) {
        new GetAttractionAsyncTask(name);
        return singleAttraction;
    }

    @Override
    public void processFinish(List<String> output) {
        mObservableAttractions = new MediatorLiveData<>();
        mObservableAttractions.postValue(output);
    }

    @Override
    public void singleResult(List<String> output) {
        singleAttraction = output;
    }
}
