package com.jlbos.androidexam;

import android.app.Application;

import com.jlbos.androidexam.api.response.AttractionTaskReponse;

public class BasicApp extends Application {

    private AppExecutor mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppExecutors = new AppExecutor();
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance();
    }

}
