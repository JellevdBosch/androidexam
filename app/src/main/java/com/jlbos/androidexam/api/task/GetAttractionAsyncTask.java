package com.jlbos.androidexam.api.task;

import android.os.AsyncTask;
import android.util.Log;

import com.jlbos.androidexam.api.response.AttractionTaskReponse;
import com.jlbos.androidexam.api.response.GetAttractionTaskResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetAttractionAsyncTask  extends AsyncTask<Void,String,String> {

    private URL mURL;
    private JSONObject jsonObject;
    private String mName;
    private List<String> attractionList;
    private GetAttractionTaskResponse delegate = null;

    public GetAttractionAsyncTask(String name) {
        jsonObject = new JSONObject();
        mName = name;
        try {
            this.mURL = new URL("https://mbodatastore.nl/attractie/pretparken.php");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... voids) {
        String result = "";

        try {
            HttpURLConnection connection = (HttpURLConnection) this.mURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Cache-Control", "no-cache");
            connection.setRequestProperty("Accept","application/json");

            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes("naam:"+mName);
            outputStream.flush();
            outputStream.close();

            connection.connect();
            int HttpResult = connection.getResponseCode();
            StringBuilder stringBuilder = new StringBuilder();
            String line = "";
            if (HttpResult == HttpURLConnection.HTTP_OK || HttpResult == HttpURLConnection.HTTP_CREATED) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();
                result = stringBuilder.toString();
            }
            connection.disconnect();
            Log.d("RESULT-----------------",result);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            JSONArray jsonArray = jsonObject.getJSONArray("attracties");

            for (int i=0;i<jsonArray.length();i++) {
                JSONObject jsonDataObject = jsonArray.getJSONObject(i);
                String name = jsonDataObject.getString("naam");
                String location = jsonDataObject.getString("plaats");
                String lat = jsonDataObject.getString("lat");
                String lng = jsonDataObject.getString("lang");
                String image = jsonDataObject.getString("img");

                HashMap<String,String> dataMap = new HashMap<>();

                dataMap.put("name", name);
                dataMap.put("location", location);
                dataMap.put("lat", lat);
                dataMap.put("lng", lng);
                dataMap.put("image", image);

                attractionList = new ArrayList<String>();
                attractionList.addAll(dataMap.values());
            }
            delegate.singleResult(attractionList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
