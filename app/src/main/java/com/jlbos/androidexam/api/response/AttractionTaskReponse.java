package com.jlbos.androidexam.api.response;

import java.util.List;

public interface AttractionTaskReponse {
    void processFinish(List<String> output);
}
