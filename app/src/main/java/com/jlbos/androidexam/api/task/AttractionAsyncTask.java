package com.jlbos.androidexam.api.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.jlbos.androidexam.api.Connectivity;
import com.jlbos.androidexam.api.response.AttractionTaskReponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class AttractionAsyncTask extends AsyncTask<Void,String,String> {

    private URL mURL;
    private JSONObject jsonObject;
    private List<String> attractionList;
    private AttractionTaskReponse delegate = null;

    public AttractionAsyncTask() {
        jsonObject = new JSONObject();

        try {
            this.mURL = new URL("www.mbodatastore.nl/attractie");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... voids) {
        String result = "";
        boolean connectionOK;
        String[] splitted = null;
//        Connectivity connectivity = new Connectivity();
//        connectionOK = connectivity.Connectivity(this.mContext);
//        if(connectionOK) {
            try {
                HttpURLConnection connection = (HttpURLConnection) this.mURL.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("Cache-Control", "no-cache");
                connection.setRequestProperty("Accept","application/json");
                if (jsonObject != null) {
                    DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
                    outputStream.writeBytes(jsonObject.toString());
                    outputStream.flush();
                    outputStream.close();
                }
                connection.connect();
                int HttpResult = connection.getResponseCode();
                StringBuilder stringBuilder = new StringBuilder();
                String line = "";
                if (HttpResult == HttpURLConnection.HTTP_OK || HttpResult == HttpURLConnection.HTTP_CREATED) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    bufferedReader.close();
                    result = stringBuilder.toString();
                }
                connection.disconnect();
                Log.d("RESULT-----------------",result);
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
//        }
        return result;
    }

    @Override
    protected void onPostExecute(String output) {
        try {
            JSONObject jsonObject = new JSONObject(output);
            JSONArray jsonArray = jsonObject.getJSONArray("attracties");

            for (int i=0;i<jsonArray.length();i++) {
                JSONObject jsonDataObject = jsonArray.getJSONObject(i);
                String name = jsonDataObject.getString("naam");
                String location = jsonDataObject.getString("plaats");
                String lat = jsonDataObject.getString("lat");
                String lng = jsonDataObject.getString("lang");
                String image = jsonDataObject.getString("img");

                HashMap<String,String> dataMap = new HashMap<>();

                dataMap.put("name", name);
                dataMap.put("location", location);
                dataMap.put("lat", lat);
                dataMap.put("lng", lng);
                dataMap.put("image", image);

                attractionList = new ArrayList<String>();
                attractionList.addAll(dataMap.values());
            }
            delegate.processFinish(attractionList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
