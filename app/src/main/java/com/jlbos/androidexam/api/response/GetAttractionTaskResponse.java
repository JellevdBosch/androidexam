package com.jlbos.androidexam.api.response;

import java.util.List;

public interface GetAttractionTaskResponse {
    void singleResult(List<String> output);
}
