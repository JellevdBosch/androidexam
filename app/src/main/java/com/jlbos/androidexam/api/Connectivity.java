package com.jlbos.androidexam.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class Connectivity {

    public boolean Connectivity(Context context) {
        ConnectivityManager check = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = new NetworkInfo[0];
        if (check != null) {
            networkInfos = check.getAllNetworkInfo();
        }
        for (int i = 0; i < networkInfos.length; i++) {
            if (networkInfos[i].getState() == NetworkInfo.State.CONNECTED) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
